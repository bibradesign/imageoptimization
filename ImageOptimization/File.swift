//
//  User.swift
//  ClientLive
//
//  Created by Waldemar Ankudin on 15.08.17.
//  Copyright © 2017 Waldemar Ankudin. All rights reserved.
//

import Foundation
class File {
    let URL: URL
    let fileName: String
    var fileSizeOld: Int
    var fileSizeNew: Int
    init(URL: URL) {
        self.URL = URL
        self.fileName = URL.lastPathComponent
        self.fileSizeOld = 0
        self.fileSizeNew = 0
    }
}

