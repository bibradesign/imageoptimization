//
//  AppDelegate.swift
//  RapidImages
//
//  Created by Vincens von Bibra on 29.06.17.
//  Copyright © 2017 Bibra Design. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(_ aNotification: Notification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


}

