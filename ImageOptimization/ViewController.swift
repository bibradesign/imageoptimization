//
//  ViewController.swift
//  RapidImages
//
//  Created by Vincens von Bibra on 29.06.17.
//  Copyright © 2017 Bibra Design. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSTableViewDelegate, NSTableViewDataSource {

    @IBOutlet var tableView: NSTableView!
    @IBOutlet weak var processButton: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.processButton.isHidden = true

        // Do any additional setup after loading the view.
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    var files: [File] = [File]()
    
    func numberOfRows(in tableView: NSTableView) -> Int {
        return self.files.count
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        if let cell = tableView.make(withIdentifier: "FileSelectionCell", owner: nil) as? FileSelectionCell {
            let file = self.files[row]
            cell.setData(file: file)
            return cell
        }
        return nil
    }
    
    func tableViewSelectionDidChange(_ notification: Notification) {
        if(self.tableView.numberOfSelectedRows == 1) {
            let answer = dialogOKCancel(question: "Remove?", text: "Remove \(self.files[self.tableView.selectedRow].fileName) from list?")
            if(answer) {
                self.files.remove(at: self.tableView.selectedRow)
                self.tableView.reloadData()
            }
        }
    }
    func dialogOKCancel(question: String, text: String) -> Bool {
        let alert = NSAlert()
        alert.messageText = question
        if(text != "") {
            alert.informativeText = text
        }
        alert.alertStyle = .warning
        alert.addButton(withTitle: "OK")
        alert.addButton(withTitle: "Cancel")
        return alert.runModal() == NSAlertFirstButtonReturn
    }
    @IBAction func addFiles(sender: AnyObject){
        let panel = NSOpenPanel()
        panel.allowsMultipleSelection = true
        panel.allowedFileTypes = ["jpg", "jpeg", "png"]
        panel.allowsOtherFileTypes = false
        panel.begin(completionHandler: {(result: Int) -> Void in
            if result == NSFileHandlingPanelOKButton {
                panel.urls.forEach({ (URL) in
                    let file = File(URL: URL)
                    do {
                        let resources = try URL.resourceValues(forKeys:[.fileSizeKey])
                        file.fileSizeOld = resources.fileSize!
                    } catch {
                        print("Error: \(error)")
                    }
                    self.files.append(file)
                })
                self.tableView.reloadData()
            }
            if(self.files.count > 0) {
               self.processButton.isHidden = false
            } else {
               self.processButton.isHidden = true
            }
        })
    }
    
    @IBAction func processFiles(sender: AnyObject) {
        let directoryPath = self.openPanel()
        if(directoryPath != "") {
            var i = 0
            for file in self.files {
                _ = self.shell(launchPath: "/bin/bash", arguments: ["-c", "/usr/local/bin/jpegtran -copy none -optimize -progressive -outfile \"\(directoryPath)/\(file.URL.lastPathComponent)\" \"\(file.URL.path)\""])
                
                do {
                    let URL = NSURL.fileURL(withPath: "\(directoryPath)/\(file.URL.lastPathComponent)")
                    let resources = try URL.resourceValues(forKeys:[.fileSizeKey])
                    file.fileSizeNew = resources.fileSize!
                    self.tableView.reloadData()
                } catch {
                    print("Error: \(error)")
                }
                i = i+1
            }
        }
    }
    
    func openPanel() -> String {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = false
        openPanel.canChooseDirectories = true
        openPanel.canCreateDirectories = true
        openPanel.canChooseFiles = false
        let i = openPanel.runModal()
        if(i == NSModalResponseOK) {
            return openPanel.url!.path
        }
        return ""
    }
    
    func shell(launchPath: String, arguments: [String]) -> String {
        let task = Process()
        task.launchPath = launchPath
        task.arguments = arguments
        
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output = String(data: data, encoding: String.Encoding.utf8)!
        if output.characters.count > 0 {
            //remove newline character.
            let lastIndex = output.index(before: output.endIndex)
            return output[output.startIndex ..< lastIndex]
        }
        return output
    }
}
