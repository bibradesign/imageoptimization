//
//  FileSelectionCell.swift
//  RapidImages
//
//  Created by Waldemar Ankudin on 30.06.17.
//  Copyright © 2017 Bibra Design. All rights reserved.
//

import Cocoa
class FileSelectionCell : NSTableCellView {
    
    @IBOutlet weak var filename: NSTextField!
    @IBOutlet weak var savedL: NSTextField!
    
    var selectionCallback : ((Bool) -> Void)?
    
    func setData(file: File) {
        self.filename.stringValue = file.fileName
        if(file.fileSizeNew > 0) {
            let savedKB = (file.fileSizeOld-file.fileSizeNew)/1000
            let savedPercent = 100 - 100 * file.fileSizeNew / file.fileSizeOld
            self.savedL.stringValue = "Saved: \(savedPercent) % (\(savedKB) KB)"
        } else {
            self.savedL.stringValue = "\(file.fileSizeOld/1000) KB"
        }
    }
}
